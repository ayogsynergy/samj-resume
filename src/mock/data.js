import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: 'Samantha Juarez', // e.g: 'Name | Developer'
  lang: '', // e.g: en, es, fr, jp
  description: '', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: '',
  name: 'Samantha Juarez',
  subtitle: "I'm a graphic designer.",
  cta: '',
};

// ABOUT DATA
export const aboutData = {
  img: 'profile.jpeg',
  paragraphOne:
    'I am a graphic designer who has experience in digital, print, and social media marketing.',
  paragraphTwo:
    "I enjoy bringing ideas to life.  With proper attention to detail, my mission is to create eye-catching graphics while staying true to a brand's identity.",
  paragraphThree:
    '"It\'s through mistakes that you actually can grow. You have to get bad in order to get good." - Paula Scher',
  resume: '', // TODO: fill in
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'usf_homecoming_logo_2020.png',
    title: 'USF Homecoming Logo',
    info: 'Revamped USF Homecoming 2020 logo for a refreshed look.',
    info2: '',
    url: 'https://www.usf.edu/student-affairs/homecoming/',
  },
  {
    id: nanoid(),
    img: 'Staircase1.jpg',
    title: 'Staircase Vinyl Design - Welcome',
    info: 'Created a vinyl design with salutatory words or phrases in over 30 different languages',
    info2: '',
    url: '',
  },
  {
    id: nanoid(),
    img: 'Staircase2.jpg',
    title: 'Staircase Vinyl Design - Go Bulls',
    info: 'Created a vinyl design with "Go Bulls" to go along the span of the staircase.',
    info2: '',
    url: '',
  },
  // TODO: add cabin with mini gallery
  {
    id: nanoid(),
    img: 'GSicon2.png',
    title: 'Favicon',
    info: 'A chique modern logo for use as a favicon.',
    info2: '',
    url: 'https://ayogsynergy.gitlab.io/chronicle/#/',
    repo: '', // if no repo, the button will not show up
  },
];

// CONTACT DATA
export const contactData = {
  cta: '',
  btn: '',
  email: '',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: nanoid(),
      name: 'twitter',
      url: '',
    },
    {
      id: nanoid(),
      name: 'codepen',
      url: '',
    },
    {
      id: nanoid(),
      name: 'linkedin',
      url: '',
    },
    {
      id: nanoid(),
      name: 'github',
      url: '',
    },
  ],
};
